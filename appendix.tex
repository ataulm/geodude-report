\begin{appendices}
\chapter{}

\section{Agent Cycle Step}
\label{sec:apdx-cycle}
Listing \ref{lst:cycleInitiate} demonstrates how the cycle step is initiated in \texttt{AgentBrain}. The \texttt{run()} method continuously calls \texttt{stepForward()}, while the agent is still alive and running. These two states (alive and running) are kept distinct to allow the possibility of an agent being suspended, but not killed.

The \texttt{AgentBrain} calls \texttt{AgentMind\#executeStep()} to retrieve a list of \texttt{Actions} (which may be empty, but ought not be null).

\begin{lstlisting}[numbers=left, float,caption={\texttt{AgentBrain}: Initiating the cycle step}, label={lst:cycleInitiate}]
@Override
public void run() {
    if (agentThread == Thread.currentThread()) {
        while (keepThreadAlive) {
            if (isAgentRunning()) {
                stepForward();
            }
        }
    }
}

/**
 * Driver behind the agent cycle
 * Tells the {@link AgentMind} to execute a cycle step.
 */
protected void stepForward() {
    if (mind != null) {
        try {
            List<Action> actions = mind.executeStep();
            if (actions != null) {
                for (Action action : actions) {
                    act(action);
                }
            }
            Thread.sleep(threadDelay);
        }
        ...            
    }
}    
\end{lstlisting}

The \texttt{AgentMind\#executeStep()} as shown in Listing \ref{lst:cycleExecute} retrieves all the unprocessed \texttt{Percepts} from the \texttt{AgentBrain}. At this stage, it decides on an \texttt{Action} or set of \texttt{Actions}, storing them in the \texttt{List<Action>}, \texttt{actionsToPerform}, finally returning this set of \texttt{Actions}. Each \texttt{AgentMind} will differ as to how it selects the \texttt{Actions} - agents are also free to store the \texttt{Percepts} in an internal data structure, which might serve as long term memory. There are no restrictions placed on the method of storage, but as stated previously with the \texttt{Container} history, it should be reasonably quick (i.e. not network storage) to ensure that the mind does not appear unresponsive.

\begin{lstlisting}[float,caption={\texttt{AgentMind}: Returning a list of \texttt{Actions}}, label={lst:cycleExecute}]
@Override
public List<Action> executeStep() throws Exception {
    List<Action> actionsToPerform = new ArrayList<>();
    List<Percept> percepts = getBrain().getAllPerceptions();
    
    ...
    
    return actionsToPerform;
}
\end{lstlisting}

When the \texttt{AgentBrain} retrieves a set of \texttt{Actions}, it passes them to the \texttt{AgentBody} one at a time, as per Listing \ref{lst:cycleActBrain} and lines 21-22 of Listing \ref{lst:cycleInitiate}.

\begin{lstlisting}[float,caption={\texttt{AgentBrain} passes \texttt{Actions} to \texttt{AgentBody}}, label={lst:cycleActBrain}]
@Override
public void act(Action action) throws Exception {
    if (action != null)
        getBody().act(action);
}
\end{lstlisting}

The \texttt{AgentBody} passes the \texttt{Actions} to an appropriate \texttt{Effector} if it has one (Listing \ref{lst:cycleActBody}), which attempts it in the \texttt{Environment} (Listing \ref{lst:cycleActEffector}).

\begin{lstlisting}[float,caption={\texttt{AgentBody} passes \texttt{Actions} to \texttt{Effector}}, label={lst:cycleActBody}]
@Override
public boolean act(Action action) {
    String actionType = action.getActionType();
    for (Effector effector : effectors.values()) {
        if (effector.handlesType(actionType)) {
            effector.act(action);
            return true;
        }
    }

    return false;
}
\end{lstlisting}

\begin{lstlisting}[float,caption={\texttt{Effector} attempts \texttt{Action} in \texttt{Environment}}, label={lst:cycleActEffector}]
@Override
public boolean act(Action action) {
    getBody().getEnvironment().attempt(getBody(), action);
    return true;
}
\end{lstlisting}

In the other direction, when an agent is notified of a particular \texttt{Action} occurring in the \texttt{Environment}, the \texttt{Container} notifies the \texttt{AgentBody} calling \texttt{AgentBody\#perceive(Action)} (Listing \ref{lst:perceive}).

\begin{lstlisting}[float,caption={\texttt{Container} notifies \texttt{AgentBody} about \texttt{Action}}, label={lst:perceive}]
@Override
public boolean perceive(Action action) {
    String actionType = action.getActionType();

    for (Sensor sensor : sensors.values()) {
        if (sensor.handlesType(actionType)) {
            sensor.sense(action);
            return true;
        }
    }

    return false;
}
\end{lstlisting}

The \texttt{Sensor} creates a \texttt{Percept} from the \texttt{Action}, then notifies the listener (\texttt{AgentBrain}) that it has a \texttt{Percept} (Listing \ref{lst:sensorsense}).

\begin{lstlisting}[float,caption={\texttt{Sensor} creates \texttt{Percept} and notifies \texttt{AgentBrain}}, label={lst:sensorsense}]
@Override
public void sense(Action action) {
    perceptions.add(new DefaultPercept(action, Calendar.getInstance()
            .getTimeInMillis()));
    if (listener != null) {
        listener.onSensorHasPercept(this);
    }
}
\end{lstlisting}

The \texttt{AgentBrain} picks up the \texttt{Percept} and adds it to its own queue (this is the queue from which the \texttt{AgentMind} takes unprocessed \texttt{Percepts}) (Listing \ref{lst:sensorHasPercept}).

\begin{lstlisting}[float,caption={\texttt{AgentBrain} takes the new \texttt{Percept}}, label={lst:sensorHasPercept}]
@Override
public void onSensorHasPercept(Sensor sensor) {
	synchronized(perceptions) {
	    perceptions.add(sensor.getPerception());
	}
}
\end{lstlisting}


\section{DefaultContainer and AbstractContainer}
\label{sec:apdx-container}
The simplest \texttt{Container} with no modifications is given by the\\ \texttt{DefaultContainer}. The \texttt{DefaultContainer} is the minimal implementation of the \texttt{AbstractContainer} and requires little modification (Listing \ref{lst:decContainer}) apart from specifying the \texttt{Physics} object (Section \ref{sec:physics}) that will govern the environment.

\begin{lstlisting}[float,caption={Declaring a new \texttt{Container}}, label={lst:decContainer}]
Container environment = new DefaultContainer("container_id",
        new DefaultPhysics());
\end{lstlisting}

This implementation supports all the required functions of a \texttt{GOLEMlite} \\\texttt{Container}:

\begin{itemize}
    \item allows \texttt{Entities} to register and deregister
    \item maintains a history of \texttt{Events} via the \texttt{ContainerHistory}
    \item allows agents to subscribe to broadcast \texttt{Events}
    \item provides a notification system for broadcast and unicast \texttt{Events} (see Section \ref{sec:actions-and-events})
\end{itemize}

\section{Defining Variables}
\label{sec:apdx-vars}
As an example, to represent the number of \texttt{MarketAgents} in the marketplace at any given time, we define a \texttt{ConbineVariable} shown in Listing \ref{lst:decConbineVar}.

\begin{lstlisting}[float,caption={Declaring \texttt{ConbineVariable}}, label={lst:decConbineVar}]
// we set a few ranges to represent different densities
ConbineVariable density = new ContinuousVariable("density");
density.addBucket(new ContinuousValueBucket("sparse", 1, 5));
density.addBucket(new ContinuousValueBucket("average", 6, 15));
density.addBucket(new ContinuousValueBucket("busy", 16, 25));
\end{lstlisting}        


The number of ranges we can set is arbitrary. Using the example given in Listing \ref{lst:decConbineVar}, \texttt{density.pickValue(\ditto average\ditto)} will return an integer between 6 and 15 inclusive.


\end{appendices}
