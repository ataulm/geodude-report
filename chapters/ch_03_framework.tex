\chapter{Framework}
\label{chap:framework}
\section{Overview}
\texttt{GOLEMlite} is a Java library, built using the concepts upon which \texttt{GOLEM} was created.

The framework requires a basic understand of the Java language, chosen because of the rise of Java (over the last ten years\footnote{\url{http://www.joelonsoftware.com/articles/ThePerilsofJavaSchools.html}}) as the primary language taught in Computer Science curriculums. Given Prolog's popularity as a taught language for courses on declarative programming paradigms, as well as its prevalence in the AI community, \texttt{GOLEMlite} follows \texttt{GOLEM's} lead in supplying a mechanism to write components of the multi-agent system in this language also, using a dependency on the \texttt{TuProlog} library\footnote{\url{http://apice.unibo.it/xwiki/bin/view/Tuprolog/}}, which provides a Prolog interpreter that runs in the JVM.

Key concepts within the framework include the environment (or container), agents (comprised of an agent body, brain and mind), and sensors and effectors.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{User Types}
\label{sec:user-types}
\texttt{GOLEMlite} suggests the separation of responsibilities when creating multi-agent systems with the framework into three parts:

\begin{itemize}
    \item Platform developers
    \item Application developers
    \item End users
\end{itemize}

The platform/framework developer is a person that contributes to the source code \texttt{GOLEMlite}. The goal of the platform developer is to make it easy for the application developer to make applications. The platform developer provides APIs for the application developer to use to make their applications. The platform developer can also make available agents and components which can be extended or used as-is by application developers.

The application developer is a person that uses \texttt{GOLEMlite} to build multi-agent systems. The goal of the application developer is to make applications (multi-agent systems) with a specific purpose for the end-user.

The end-user (who may not necessarily a full-stack developer themselves) is a person that uses the applications built by the application developer. This user downloads and installs \texttt{GOLEMlite}, and downloads applications (separately) which can be run on \texttt{GOLEMlite}, by copying the application into a designated plugins directory. The end-user is able to run experiments with different agent strategies, and get results. The end-user may modify strategies, and substitute the built-in strategies in a given application if the application developer has given provisions for this in the form of a configuration/setup screen, for agents that the application developer has designated (i.e. there may be some agents with fixed, closed strategies, unknown and unchangable by the end-user).

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Agents}
An agent in \texttt{GOLEMlite} is a concept comprised of three distinct components: the agent body, agent mind, and agent brain. These are components adopted from \texttt{GOLEM} and its ancestor project, \texttt{PROSOCS} \cite{stathis2004prosocs}.

The agent body is the physical representation of the agent in the environment; sensors and effectors, which act upon and receive input from the environment, belong to the body.

The agent mind is the reasoning component of the agent. It processes information - either information received immediately prior to the processing, or received and stored over time - and returns a set of actions to perform.

The agent brain is the mechanical component which ties the body and mind together. It continuously requests the mind for new actions (regardless of whether there are new perceptions), sending returned actions to the body (which in turn, sends it to the first effector that can perform that action, otherwise does nothing). Figure \ref{fig:agent-arch} shows the compositional relationship between the different components which make an agent in \texttt{GOLEMlite}, where the arrow direction specifies a "belongs-to" or "is-referenced-in" relationship, e.g. the brain has a reference to both the mind and the body, but the body is not aware of the mind, and vice-versa.

Sections \ref{sec:app-agents} to \ref{sec:hybrid-agents} give an overview of different agents which can be classified in \texttt{GOLEMlite}. These classifications are used to help assign responsibilities to different groups of agents, to help identify their roles and the boundaries of their purpose within the application they work in. By identifying the responsibilities and roles of particular agents and behaviours, it is easier for the application developer to implement them into an application, deciding which (if any) agents should be allowed strategies that can be modified by the end user, for example.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics{figures/agent-arch.pdf} 
        \caption{GOLEMlite Agent Architecture - arrows indicate "is-referenced-in"/"belongs-to" relationship}
        \label{fig:agent-arch}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=25em]{figures/standardAgentPackage.png} 
        \caption{Agent Class hierarchy}
        \label{fig:agentpackage}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

% --------------------------------------------------------------------------- %

\subsubsection{Application Agents}
\label{sec:app-agents}
Application agents are agents that are only aware of the virtual world they are part of; as far as they are concerned, there is no other platform or container outside the environment they interact in.

These are the agents that will typically be given strategies by end-users (refer Section \ref{sec:user-types} for information on user types), and will interact with other agents and objects within the environment they are situated in.

% --------------------------------------------------------------------------- %

\subsubsection{Infrastructure Agents}
\label{sec:infrastructure-agents}
We define infrastructure agents as agents that act on behalf of the environment. Their goal is to help ensure the continued health of the underlying system, that is, helping to ensure that the system meets its intended design specification. This may include acting on behalf of the platform developer to optimise the use of the platform (system resources, where these may be given to the platform for example) when running multiple applications simultaneously. A concrete example is given in Section \ref{sec:marketbroker}.

Infrastructure agents are developed by platform developers and application developers only - end users should not be able to supply a custom strategy for these agents; if it’s the case that it’s appropriate for the end user to be able to do this, then the agents in question are likely not infrastructure agents.

Application developers should use infrastructure agents to help achieve their goal (make working applications). This involves assigning the infrastructure agents responsibility for certain tasks, like collecting a particular metric, or entrusting them to coordinate application-oriented agents to their goals, like a knowledge broker agent (see \ref{sec:hybrid-agents}). Infrastructure agents are not biased to any particular application agent. E.g. an infrastructure agent responsible for guaranteeing the delivery of private messages in the environment should consistently ascribe the property of non-repudiation to every successful delivery for both the sender and recipient.

Infrastructure agents in \texttt{GOLEMlite} have access to the \texttt{ContainerHistory} and the register of \texttt{Entities} in the \texttt{Container}. Infrastructure agents may be domain independent, for example agents responsible for balancing the available system resources to various \texttt{Containers} running on the same platform.

% --------------------------------------------------------------------------- %

\subsubsection{Hybrid Agents}
\label{sec:hybrid-agents}
A hybrid agent is an infrastructure agent that exists primarily within the environment, and interacts with application-oriented agents. Again, their strategies are fixed by the application-developer (not the platform-developer) but they are domain-dependent; their main function is to provide services to application-oriented agents, like an information broker that can respond to domain-specific queries, such as "where can I find a store that sells laptops?", for instance.

This classification exists to identify the agents which perform a required role, or provide an essential service to other agents in the environment, but are themselves not the interest of the application - for example, in an application where agents are separated into teams, and tasked with performing some objective, there may be a hybrid agent which gives directives to the two teams (e.g. "collect as many red balls as possible in 30 seconds") - in this instance, the end-user is concerned with the performance of the agents on the two teams, and the actions of the hybrid agent (so long as it performs its duties) are of no consequence.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Environment}
\label{sec:env}
The environment is represented by a container in \texttt{GOLEMlite}. This is a layer in which software agents exist, and is a model of the physical world (if such a comparison is required), in which the agent can effect actions using its effectors, and sense events using its sensors. Agents can interact with other agents and objects that are within the same environment; in Figure \ref{fig:env-basic} agents \texttt{a1} and \texttt{a2} can send and receive messages to each other, and perform actions on objects \texttt{o1} and \texttt{o2}. While agent \texttt{a3} has been created, it is not aware of any of the entities in environment \texttt{e}.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/environment-basic.pdf} 
        \caption{Agents within and without an environment}
        \label{fig:env-basic}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Figure \ref{fig:env-addnew} shows agent \texttt{a3} is added to environment \texttt{e} using the \\\texttt{makePresent(Entity)} function. This adds agent \texttt{a3} to the list of entities in environment \texttt{e} which will be notified about events that occur in the environment (with further constraints), and qualifies the entity to attempt to perform actions in the environment. Objects are not currently featured in \texttt{GOLEMlite} (hence the parentheses), but they are considered to be much the same as in \texttt{GOLEM} - purely reactive entities with no autonomous behaviour. Figure \ref{fig:container} shows the class diagram of the \texttt{Container} interface.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/environment-addnew.pdf} 
        \caption{Adding an agent to the environment}
        \label{fig:env-addnew}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Actions in a given environment are governed by the physics of that environment. This component decides which actions are permissable at any given time in the environment; when an action is attempted, the physics component decides whether it's safe to assert this action as an event, or whether to mark the attempt as a failure because some constraint has not been met (nothing is asserted in this case).

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Sensors and Effectors}
\label{sec:sen-eff}
Sensors and effectors are components which are registered to a particular agent body. Sensors can perceive events of a particular type, and effectors can perform actions of a particular type. A body typically has a single sensor or effector of a given type; currently, when an event is perceived, it'll be consumed by the first sensor that can handle that type, and when an action is decided upon, it will be performed by the first available, compatible effector - there is currently no mechanism to have redundancy.

In \texttt{GOLEMlite}, each of these components typically has a unique purpose - an "ear" would be able to perceive \texttt{Events} of type "speak", for example. Application developers are able to design components which act as both a \texttt{Sensor} and an \texttt{Effector}, like a two-way radio, that can produce \texttt{Actions} of type "radio-broadcast", as well as receive and interpret them too.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Environment as a Container}
In \texttt{GOLEMlite}'s architecture, the environment is fronted by two interfaces: the \texttt{Container} and the \texttt{Environment}. \texttt{Environment} is the standard interface used by most agents, whereas \texttt{Container} is a child of \texttt{Environment} with a few extra features, and is used by the application as well as infrastructure agents (as mentioned in Section \ref{sec:infrastructure-agents}).

The \texttt{Environment} allows agents to attempt \texttt{Actions} and also lets agents subscribe to broadcast \texttt{Events} (more on this in Section \ref{sec:actions-and-events}).

The \texttt{Container} interface is more low-level, with the ability to register and deregister entities (\texttt{makePresent(Entity)} and \texttt{removeEntity(String)}). \\\texttt{Entities} (agents and objects) are registered with the \texttt{Container}, after which they can interact with other \texttt{Entities} which have also been registered (as mentioned in Section \ref{sec:env}). The \texttt{Environment} is the virtual representation of the physical world in which agents are situated, for example, a maze for an application testing path-finding agents, or a smart-home for a multiagent system given the task of managing the state of a house. It allows agents to attempt \texttt{Actions}, and to subscribe to (be notified of) different \texttt{Events} which occur within the \texttt{Environment}.

In addition to this, the \texttt{Container} interface also has methods dealing with the history of the environment (Section \ref{sec:con-hist}). An example of creating a basic \texttt{Container} is given in the appendix in Section \ref{sec:apdx-container}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Container History}
\label{sec:con-hist}
The environment in \texttt{GOLEM} is represented (for the most part) in Prolog, where the container history is the collection of all assertions. These assertions represent changes to the state of the environment from the initial state; the current state of the environment can be calculated based on the cumulutative effect of every assertion.

In \texttt{GOLEMlite}, where the environment can be represented using a Java-based container or a Java/Prolog container (as in \texttt{GOLEM}), the \texttt{Container} interface specifies the some functions relating to the history:

\begin{itemize}
    \item \texttt{clearHistory()} - which clears all assertions from the history
    \item \texttt{clearAll()}, as \texttt{clearHistory()}, also deregisters all \texttt{Entities} from the \texttt{Container}
    \item \texttt{assertEventInHistory(Event)} - asserts an \texttt{Event} to the history
\end{itemize}

It is left to the developer to decide how to store the history; the call to \\\texttt{assertEventInHistory(Event)} may convert the \texttt{Event} to a form that can be asserted in a Prolog database, or if the environment is developed entirely in Java, a default implementation is given, with the use of a \texttt{ContainerHistory} object.

\texttt{ContainerHistory} represents an ordered collection of \texttt{Events}, a wrapper around a synchronised \texttt{ArrayList<Event>} structure. It is a simple wrapper, allowing tail insertions, retrieval by index and has a copy function to create a duplicate \texttt{ContainerHistory} for traversal without blocking new insertions.

Apart from this, it may be stored in a local database on the filesystem, but it should be relatively quick to access and modify (for example, a history stored on a remote server with insertions and queries performed by network calls would not be a good idea).

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Physics}
\label{sec:physics}
The \texttt{Physics} component governs the \texttt{Actions} which are allowed to occur in an \texttt{GOLEMlite} \texttt{Container}. These constraints ensure that the state of the environment is kept in a valid condition. \texttt{GOLEM} defines \texttt{Physics} as a:

\begin{quote}
"set of physical laws specifying the possible evolutions of the environment, including how these evolutions are perceived by agents and affect objects and processes in the environment" \cite{golem:eemas07}
\end{quote}

When an \texttt{Action} is attempted, the \texttt{Container} chains the \texttt{Action} to the associated \texttt{Physics} object where it is processed, and returns a boolean value indicating whether or not the \texttt{Action} is possible. This is shown in Figure \ref{fig:attempt-action}. 

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/attempt-action.pdf} 
        \caption{Attempting an Action}
        \label{fig:attempt-action}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

The simplest implementation of \texttt{Physics} is given in \texttt{DefaultPhysics} - it returns \texttt{true} for all calls to \texttt{isPossible(Event)}. It is preferred that the application developer create a domain-dependent implementation of the \texttt{Physics} class, at the very least, ensuring the \texttt{Events} contain \texttt{Actions} of a set of allowed types (see Section \ref{sec:actions-and-events}).

\texttt{GOLEM's} \texttt{Physics} was a wrapper over a Prolog interpreter; while this isn't demonstrated, it is certainly possible to implement in \texttt{GOLEMlite}. In this case, it may be necessary for the \texttt{Physics} component to be able to access the \\\texttt{ContainerHistory}; \texttt{GOLEMlite} is flexible enough to allow the \texttt{Physics} component to be part of the \texttt{Container} (as is the case in \texttt{GOLEM}), so accessing the state of the environment becomes trivial, because the \texttt{Container} and \texttt{Physics} object would be sharing the same database. In other cases, for example a Java-based \texttt{Container} and Prolog-based \texttt{Physics}, it would be required to provide some mechanism of allowing the \texttt{Physics} object to access the state of the environment by giving access to \texttt{ContainerHistory}. \texttt{TuProlog} is able to call functions from Java objects but this scenario is not currently supported in \texttt{GOLEMlite}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{AgentBody}
\label{sec:agent-body}
The \texttt{AgentBody} is the \texttt{Entity} which is made present in the environment. It stands as the physical component that other agents are able to perceive and interact with, being the summation of a set of sensors and effectors (Section \ref{sec:actions-and-events}).

The \texttt{AgentBody} is the interface between the \texttt{AgentBrain} and the \\\texttt{Environment}. While in \texttt{GOLEM}, the \texttt{AgentBody} was responsible for controlling the agent's thread, \texttt{GOLEMlite} demotes the body to a mechanical shell with no significant business logic; it's a tool which the \texttt{AgentMind} can utilise to achieve the agent's goals.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{AgentMind}
\label{sec:agent-mind}
The \texttt{AgentMind} forms the consciousness of the agent, the component which is capable of reasoning. The purpose of this component is to decide the next set of \texttt{Actions} that the agent should perform.

It may do this reactively, only taking into account the most recent \texttt{Percept}, or it may maintain a memory of \texttt{Percepts}, and decide based on these.

A Prolog-based implementation of the \texttt{AgentMind} is given in \\\texttt{PrologAgentMind}. It assumes the existence of a goal which returns a list of \texttt{Actions} (where \texttt{Actions} are of a particular String format), typically \\\texttt{\ditto select(Actions).\ditto}, though the name of this goal may be set by overriding \\\texttt{PrologAgentMind\#requestListOfActions(String)}.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=25em]{figures/container.png} 
        \caption{The \texttt{Container} Interface}
        \label{fig:container}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{AgentBrain}
\label{sec:agent-brain}
The \texttt{AgentBrain} is the component which connects an \texttt{AgentBody} (Section \ref{sec:agent-body}) and \texttt{AgentMind} (Section \ref{sec:agent-mind}). There is no direct access of the \texttt{AgentMind} from the \texttt{AgentBody} (or vice versa) except through the \texttt{AgentBrain}.

In \texttt{GOLEM}, the \texttt{AgentBody} contains the agent thread, and calls \\\texttt{AgentMind\#executeStep()} repeatedly until the agent is stopped. In \texttt{GOLEMlite}, we opted to move the agent thread to the \texttt{AgentBrain}; from a metaphorical standpoint, the brain is the driving force of both mechanical actions from the body, and conscious thought from the mind.

Apart from this difference, the \texttt{AgentBrain} has not changed much in functionality - it still serves as a first-in-first-out queue for both sensory input (to be delivered to the \texttt{AgentMind} from the sensors attached to the \texttt{AgentBody}) and \texttt{Action} out (to be delivered to the \texttt{AgentBody) from the \texttt{AgentMind}}. Figure \ref{fig:agentbrain} shows the \texttt{AgentBrain} interface.

As the class hierarchy shows in Figure \ref{fig:agentpackage}, there are two default implementations of the \texttt{AgentBrain} given. The reasoning for the addition of \texttt{PoolableAgentBrain} and \texttt{DefaultPoolableAgentBrain} is discussed in Section \ref{sec:changesFromConbine}.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/cycle-step.pdf} 
        \caption{Agent's cycle-step}
        \label{fig:cycle-step}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Figure \ref{fig:cycle-step} represents two basic flows in the agent's cycle step. It is discussed in more detail with code examples in the Appendix, Section \ref{sec:apdx-cycle}.

Decide-action cycle:

\begin{itemize}
    \item \texttt{AgentBrain} calls \texttt{AgentMind\#executeStep()}
    \item \texttt{\#executeStep()} retreives all unprocessed \texttt{Percepts} from \texttt{AgentBrain}
    \item \texttt{\#executeStep()} decides on a series of \texttt{Actions}, passes them back to the \texttt{AgentBrain}
    \item \texttt{AgentBrain} calls \texttt{AgentBody\#act(Action)} for each \texttt{Action} returned

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=25em]{figures/AgentBrain.png} 
        \caption{The \texttt{AgentBrain} Interface}
        \label{fig:agentbrain}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Sensing new \texttt{Percepts}:

\begin{itemize}
    \item \texttt{Container} calls \texttt{AgentBody\#perceive(Action)} to notify about an \\\texttt{Action}
    \item \texttt{AgentBody} passes it, using \texttt{Sensor\#sense(Action)} to pick it up
    \item \texttt{Sensor} calls \texttt{AgentBrain\#onSensorHasPercept(Sensor)} to indicate it has a new \texttt{Percept}
    \item \texttt{AgentBrain} adds this \texttt{Percept} to a queue of \texttt{Percepts}, "Perceptions to Process"
\end{itemize}

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Sensors, Effectors, Actions and Events}
\label{sec:actions-and-events}
\subsection{Actions}
An \texttt{Action} in \texttt{GOLEMlite} is a Java object consisting of three components:

\begin{itemize}
    \item an \texttt{ActionType} (essentially a String)
    \item an optional recipient ID
    \item a tuple which constitutes the payload
\end{itemize}

The \texttt{ActionType} was intended to act similarly to the Action component of an \texttt{Intent} from the Android\textsuperscript{\texttrademark} platform:

\begin{quote}
"The action largely determines how the rest of the intent is structured — particularly the data and extras fields — much as a method name determines a set of arguments and a return value. For this reason, it's a good idea to use action names that are as specific as possible, and to couple them tightly to the other fields of the intent. In other words, instead of defining an action in isolation, define an entire protocol for the Intent objects your components can handle."\footnote{\url{http://developer.android.com/guide/components/intents-filters.html\#iobjs}}
\end{quote}

As a string, it doesn't contribute to \texttt{Action} in any programmatical way, but it does indicate the structure, and intended use, of the payload. \texttt{GOLEMlite} currently provides a single built-in \texttt{ActionType} ("SPEAK"), which defines a tuple consisting of a single string component representing the words spoken. Each \texttt{ActionType} should define two things: the components of the tuple (types, and in which order they occur), and what each of the components represent.

Application developers are encouraged to develop their own \texttt{ActionTypes} but in order to prevent any confusion or name clashes, the value of an \texttt{ActionType} should be prefixed with some unique identifier - the Java package name is a suitable candidate for prefixing the type with, for example, in \texttt{GOLEMlite}, \texttt{ActionType.SPEAK} resolves to \texttt{\ditto uk.ac.rhul.cs.dice.golem.action.speak\ditto}.

The optional recipient ID refers to whether the \texttt{Action} is meant as a broadcast or a unicast. A broadcast is an \texttt{Action} without a specified recipient - the \texttt{Action} is attempted on the \texttt{Environment}, and any \texttt{Sensor} that can interpret that \texttt{Action} (based on \texttt{ActionType} as discussed) is \textit{able} to perceive it. Unicasts (colloquially referred to as "whispers" in the project), specify a recipient, by \texttt{Entity} ID. \texttt{Actions} delivered as a unicast message are typically just that, a message, e.g. speak actions.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Events}
An \texttt{Event} is simply an \texttt{Action} with a timestamp and the ID of the \texttt{Entity} that instigated the \texttt{Action}. The timestamp refers to the time point that the \texttt{Action} was asserted in the \texttt{ContainerHistory}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Sensors}
A \texttt{Sensor} is a component that can interpret \texttt{Actions}. When it perceives an \texttt{Action} (the \texttt{Container} notifies the \texttt{AgentBody} that it has a message, and the \texttt{AgentBody} chains to the \texttt{Sensor}), it wraps the \texttt{Action} into a \texttt{Percept}.

A \texttt{Percept} is similar to an \texttt{Event}, and contains the \texttt{Action} and a timestamp, but the timestamp refers to the time point at which the \texttt{Sensor} received the message. This is because the \texttt{Sensor} is only aware of the \texttt{Action} when it perceives the \texttt{Action}; as far as it knows, the \texttt{Action} has just occurred. Similarly, there is no way for the \texttt{Sensor} to know which \texttt{Entity} produced the \texttt{Action}.

When a \texttt{Sensor} is constructed, it registers for the \texttt{ActionTypes} that it can interpret (Listing \ref{lst:decSensor}).

\begin{lstlisting}[float,caption={Creating a Sensor}, label={lst:decSensor}]
public class Ear extends AbstractSensor {
    public Ear(String id, AgentBody context) {
        super(id, context);
        addType(ActionType.SPEAK.toString());
    }
}
\end{lstlisting}

The aim is that a library of prebuilt \texttt{Sensors} (and \texttt{Effectors}) will ship with \texttt{GOLEMlite} so application developers can either use these out of the box, or extend them further (see Listing \ref{lst:extendSensor}), where the extended implementation retains all the abilities of its parent.

\begin{lstlisting}[float,caption={Extending a Sensor}, label={lst:extendSensor}]
public class SuperEar extends Ear {
    public SuperEar(String id, AgentBody context) {
        super(id, context);
        addType(ActionType.WHISPER.toString());
    }
}
\end{lstlisting}

When an \texttt{Event} is asserted, the \texttt{Container} decides whether a particular agent is meant to be notified about the occurrence.

For unicast \texttt{Actions}, the \texttt{Container} will blindly call \\\texttt{AgentBody\#perceive(Action)}. Whether the recipient is able to interpret the message is of no consequence to the \texttt{Container} (nor indeed the recipient). A metaphor is used to illustrate this case: assume person A is attempting to communicate with person B by flashing an infrared light in person B's direction. As person B is unable to see the light (he has no capable sensors) person B ignores it, and the message is not received - if person A wants to communicate with person B, it is person A's responsibility to ensure that it communicates in a method that person B understands.

For broadcast \texttt{Actions}, the \texttt{Container} will call \\\texttt{AgentBody\#perceive(Action)} on every \texttt{AgentBody} that has at least one \\\texttt{Sensor} that has \textit{registered} to receive broadcast \texttt{Actions}. The default setting is that \texttt{Sensors} can only receive private messages (unicast); if an agent wants to listen for all \texttt{Actions} of a specified type, it must explicitly register to do so.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Effectors}
An \texttt{Effector} works in a similar fashion to \texttt{Sensors} in that during construction, it must also declare the \texttt{ActionTypes} that it is capable of producing (Listing \ref{lst:decEffector}).

\begin{lstlisting}[float,caption={Creating an Effector}, label={lst:decEffector}]
public class Mouth extends AbstractEffector {
    public Mouth(String id, AgentBody context) {
        super(id, context);
        addType(ActionType.SPEAK.toString());
    }
}
\end{lstlisting}

When the \texttt{AgentMind} decides on a set of \texttt{Actions} to produce, the \texttt{AgentBrain} sends them one at a time to the \texttt{AgentBody} via \texttt{AgentBody\#act(Action)}. This function will return a boolean indicating whether the \texttt{AgentBody} has an \texttt{Effector} which is \textit{capable} of performing such an \texttt{Action}, but no guarantee is given as to the success of performing that \texttt{Action}.

From the \texttt{AgentBody}, it iterates through its list of \texttt{Effectors}. If it finds an \texttt{Effector} that is capable of performing the \texttt{Action}, it calls \\\texttt{Effector\#act(Action)} and returns true, otherwise returns false.

The \texttt{Effector} calls \texttt{Environment\#attempt(Entity, Action)}, passing a reference to the \texttt{AgentBody}, which provides some form of non-repudiation: if the \texttt{Event} is asserted, the ID of this agent will be recorded in the \texttt{ContainerHistory}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Summary}
The work on \texttt{GOLEMlite} presents a set of organised packages of related classes, which have been named descriptively, according to a strict naming convention. The codebase is free from "dead" code (unused code or code which is commented out).

The \texttt{GOLEMlite} framework adds base concepts like Infrastructure Agents, entities with who have complete access to the \texttt{Container}, in contrast to Application Agents, that can only access the \texttt{Container} via a limited interface (the \texttt{Environment}).

The changes made in this project lays the foundation for shipping libraries of components with the initial release of \texttt{GOLEMlite}. The intention is that application developers will be able to use the components out of the box (like \texttt{DefaultAgentBody} for example). Components like \texttt{Sensors} and \texttt{Effectors} are extensible and combinable by design. A simple but common use-case for something like this is to produce \texttt{JADE}-like agents, i.e. ones that can speak and listen, out of the box using something like "\texttt{new PseudoJADEBody()}".

The framework is built using Maven\footnote{\url{http://maven.apache.org/}}, unlike \texttt{GOLEM} which used the internal build tool from Eclipse\footnote{\url{http://www.eclipse.org/}}. Using Maven aids progress towards two goals:

\begin{itemize}
    \item Simplifies dependency management - it's easier to see which dependencies \texttt{GOLEMlite} relies on as they are all declared in the POM file (a project manifest), as well as the version of each dependency.
    \item Encourages developer contribution - using Maven (the de facto, but not official, standard for managing Java projects) means that it's possible to work on the framework in whichever integrated development environment (IDE) is most comfortable for the developer; most major IDES (including Eclipse, Intellij IDEA, and NetBeans) support Maven projects.
\end{itemize}

In line with the second goal, the project has been pushed to and is now maintained on BitBucket\footnote{\url{https://bitbucket.org/dice_rhul/golemlite/overview}}, a social coding platform based on the Git\footnote{\url{http://git-scm.com/}} protocol, which provides a low barrier of entry for new developers to contribute code (using a fork and pull model) \cite{dabbish2012social}.
