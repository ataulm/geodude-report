\chapter{Introduction}
\label{chap:intro}
Intelligent Agents are software entities that work with the objective of achieving some set goal, in a given environment. We use the "weak notion of agency" as described by Wooldridge \cite{wooldridge1995intelligent}, which describes an agent as a software-based computer system with properties including autonomy, social abilility (can communicate with other agents), reactivity (responds to events in the environment) and pro-activeness (is able to act on its own initiative, i.e. not only to stimuli in the environment).

The domain of applicability for agent-based design ties closely with areas in which artificial intelligence is relevant; agents are used to model actors and decision makers, or can be used to compartmentalise areas of a system, grouping them into sections of responsibility (where an agent or set of agents is responsible for each section). Agent-based design allows problems to be tackled in smaller, manageable blocks, and the use of agents allow delegation of responsibility.

\section{Motivation}
The motivation for developing \texttt{GOLEMlite} stems from a desire to provide a simple framework which can be used by developers new to agent programming (including students), to demonstrate the application of multi-agent systems with cognitive architectures.

\texttt{GOLEMlite} is built from scratch, based on some of the concepts developed in \texttt{GOLEM} - Generalised OntoLogical Enviroments for Multi agent systems\footnote{\url{http://golem.cs.rhul.ac.uk/}}. While part of \texttt{GOLEM}'s objectives is to further develop the existing components and add more advanced features, \texttt{GOLEMlite} is focused on paring down to the basic components which make up a multi-agent system, with the intention of making it as easy as possible for developers yet still remaining flexible enough for researchers to develop applications in which they can test and compare their strategies.

When development on \texttt{GOLEMlite} began, \texttt{GOLEM} was not an ideal platform for developing multi-agent systems. It was difficult to reconcile the implementation with papers written \texttt{GOLEM}, and there was no official documentation about how to use the framework except for in-line comments which were often cryptically worded or no longer relevant to the code it appeared near. This led to a library with tightly-coupled components, and with an inconsistent naming convention making it difficult to discern domain independent code from code used for tests and demonstrations.

It must be remembered that the \texttt{GOLEM} project was built as a \textit{prototype} platform \cite{golem:eemas07}, and was not developed for use by third-party developers. \texttt{GOLEMlite}, on the otherhand, is developed with the express aim of being used externally by third-party developers.

\section{Aims and Objectives}
The main aim of this thesis is to develop a version of \texttt{GOLEM} suitable to be used as a framework for multi-agent applications running on a single machine (initially). The emphasis is placed on the software engineering aspects of the project, and less so on the AI aspect; \texttt{GOLEMlite} will be adhering to principles which underpin \texttt{GOLEM}.

The following outlines the set of objectives which should be reached upon successful conclusion of the project:

\begin{itemize}
    \item Produce a framework (lighter than \texttt{GOLEM}) which can be used to develop multi-agent systems, based on concepts upon which \texttt{GOLEM} was built
    \item Keep the API simple, preferring ease-of-use to features
    \item Demonstrate the framework in action by building a non-trivial application (Chapter \ref{chap:conbine})
    \item Evaluate the framework with these objectives in mind, with the purpose of guiding the development of the next version or refinement of the \texttt{GOLEMlite} framework
\end{itemize}

\section{Contributions}
The contributions made through the \texttt{GOLEMlite} project which are demonstrated in this thesis include the following:

\begin{itemize}
    \item Introduction of an application architecture to \texttt{GOLEM}, allowing distribution of applications separate from the platform executable
    \item Provide a set of components which ship with the framework, including libraries of \texttt{Sensors} and \texttt{Effectors} which can be extended and combined to each other to form new capabilities
    \item Extends development concepts, including the separation of concerns for different \texttt{GOLEM} users: user, application developer, and platform developer
    \item \texttt{GOLEMlite} consolidates (and refactors) concepts from the \texttt{GOLEM} project into a coherent framework, which can be used as a standalone library or used to create \texttt{GOLEMlite} plugins
\end{itemize}

\section{Outline}
This thesis begins by presenting some background information about the \texttt{GOLEM} project in Chapter \ref{chap:background}, where an introduction to the concept of Agents and Environments will be given in Section \ref{sec:iamas}. Section \ref{sec:platforms} gives a cursory overview on the \texttt{JADE}, \texttt{Jason} and \texttt{JaCaMo}, followed by a brief introduction to the \texttt{GOLEM} project in Section \ref{sec:bg-golem}.

Chapter \ref{chap:framework} describe the \texttt{GOLEMlite} framework in its entirety, and is followed by a look at the implementation of the \texttt{ConBiNe} application in Chapter \ref{chap:conbine} which will demonstrate the use of \texttt{GOLEMlite} in a practical project.

The evaluation in Chapter \ref{chap:evaluation} will describe the problems and issues faced during development of \texttt{ConBiNe} and \texttt{GOLEMlite}. Finally, future work will be described, detailing areas in which \texttt{GOLEMlite} can be improved in the discussion in Chapter \ref{chap:discussion}.
