\chapter{Case study: ConBiNe}
\label{chap:conbine}
\section{Introduction}
\label{sec:conbineintro}

To demonstrate the use of \texttt{GOLEMlite} in a useful setting, we implemented a testbed for negotiation strategies called \texttt{ConBiNe}. The application was built to test a strategy created for the proposed domain, by Bedour Alrayes.

\texttt{ConBiNe} (\textbf{Con}current \textbf{Bi}lateral \textbf{Ne}gotiations) is a simulation testbed designed to compare and analyse the benefits of using a particular negotiation strategy over another, where the domain is set as an electronic marketplace, in which human (or corporate) users are represented by software agents, with the goal to either buy or sell a particular product in an unattended fashion - the user gives a set of constraints to the agent and the agent aims to resolve its task with as high a utility as it can manage.

Buyer and seller agents are collectively classed as \texttt{MarketAgents}, and negotiate with each other in an \textit{open marketplace} - a marketplace where \texttt{MarketAgents} are free to enter and leave at any time, that is, not all participants are necessarily known when the marketplace \textit{opens}.

The movivation behind such a marketplace is derived from existing services which require human user attention during the negotiation process, which consumes time and is susceptible to lapses of judgement (e.g. impulse purchasing or mistakes due to absentmindedness). Existing online auctions lack the functionality to allow concurrent negotiations, for example, eBay considers a bid to be a binding contract between the buyer and seller; it becomes imprudent for the buyer to bid on multiple items running at the same time. 

Limitations in the existing area (of negotiation) include an unsuitable agent architecture for concurrent negotiation; existing architectures do not effectively model the self, the opponent and the environment in such a way as to aid the symbolic representation of decision making. \cite{Bedour2013a, Alrayes2013} Further, existing protocols do not lend themselves well to the process of concurrent negotiation and there are no existing methods to pre-compute an \textit{optimal} strategy at design time.

The aim of Alrayes' work is to: 

\begin{quote}
"Study the usefulness of multi-agent systems to support e-marketplaces to allow multiple concurrent negotiations."
\end{quote}

The role of \texttt{GOLEMlite} was to build such a multi-agent system that would allow end-users to drop in their agent strategies, to compare and contrast them against others.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Domain and terminology}
To avoid confusion, a set of terms used in the remainder of this chapter will be defined, or explicitly stated here.

A \texttt{MarketAgent} refers to a software agent which has the role of a buyer, or the role of a seller. A buyer is a software agent whose goal is to purchase a product according to the constraints given to it by a human user. In this application, a buyer will only be purchasing a single product, and will consider its goal achieved when it has purchased this product. A seller is a software agent whose goal is to sell products. In this application, sellers are assumed to carry an infinite stock of whichever product they are initialised with.

A \textit{negotiation} is defined as the set of messages, related to a specific product (for sale), between a single buyer and a single seller - a bilateral dialogue, beginning with the buyer's initial offer, and ending when one or both of the parties leaves the dialogue, which may be due to a successful conclusion (where the buyer sends an "accept" message) or an unsuccessful conclusion, where either of the parties sends an "exit" or "decommit" message (each of which terminates the negotiation). Each negotiation, or dialogue, is given a unique identifier which is used to refer to that dialogue. With this unique dialogue ID, it becomes possible for the buyer and seller to engage in multiple dialogues together.

The \textit{utility} is defined as a value between 0 and 1 inclusive, representing the value of an outcome to a \texttt{MarketAgent}, where the aim is to maximise the utility of a negotiation outcome. The parameters which are used to calculate the utility may differ from strategy to strategy, for example, buyer-a may consider the quickest successful conclusion to be of the highest utility, where as buyer-b may consider the cheapest successful conclusion to be of the highest utility, and further, buyer-c will consider some combination of price and time in its calculation of utility for the outcome of a negotiation.

The \textit{marketplace} refers to the virtual environment in which negotiations take place. The "open" in "open marketplace" references the property of the marketplace that allows buyers and sellers to enter and leave at any point in the marketplace's lifetime.

The \textit{buyer's deadline} is the duration of time, in seconds, that the buyer has to conclude its task. The buyer starts an internal timer from the moment it enters the marketplace, and it must stop negotiating when its deadline is reached. If it has not reached a successful outcome for the negotiation at this point, it will leave the marketplace with an unsuccessful outcome.

The \textit{seller's deadline} is the duration of time, in seconds, that the seller is prepared to spend in a particular negotiation. The seller starts an internal timer (for each negotiation it is a part of) from the moment it receives an initial offer from its opponent (a buyer) and will exit the negotiation when the deadline is reached. It will not leave the marketplace based on this deadline.

A \textit{run} is defined as a series of \texttt{Events} which starts when the first \texttt{MarketAgent} enters the marketplace, to the moment the last \texttt{MarketAgent} leaves the marketplace.

We use the term \textit{simulation} to refer to a collection of runs in a given experiment, where each run may be created with the same parameters (repetitions for empirical evaluation) or different parameters (to gauge the effect of changing parameters on the outcome).

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Concurrent Alternating Offers Protocol}
\label{sec:caop}
The standard alternating offers protocol \cite{rubinstein1982perfect} included the following actions:

\begin{itemize}
    \item \texttt{offer(x)} (where \texttt{x} is the value of the offer)
    \item \texttt{accept}
    \item \texttt{reject} (followed by a counter-offer)
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/aop-bedour.pdf} 
        \caption{Alternating Offers Protocol State Diagram}
        \label{fig:aop-state}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

The state diagram for this protocol for a bilateral negotiation is shown in Figure \ref{fig:aop-state} and demonstrates there are two players, with unlimited alternating offers - the negotiation continues until one player accepts an offer.

This protocol does not support concurrent bilateral negotiations; similar to the eBay example in Section \ref{sec:conbineintro}, entering a negotiation implies commitment to that negotiation. Alrayes proposes an extended protocol, the Concurrent Alternating Offers Protocol.

This extends the existing protocol, and contains the following actions:

\begin{itemize}
    \item \texttt{offer(x)}
    \item \texttt{accept}
    \item \texttt{commit}
    \item \texttt{decommit}
    \item \texttt{exit}
\end{itemize}

The state diagram for the extended protocol is shown in Figure \ref{fig:caop-state}.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/concurrentaop-bedour.pdf} 
        \caption{Concurrent Alternating Offers Protocol State Diagram}
        \label{fig:caop-state}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

As in the original protocol, the buyer initiates the negotiation by sending an initial offer and then the two parties exchange messages alternately. At any stage after the initial offer, either party is able to "exit", or if a \textit{commitment} has been made, "decommit", which results in a failed negotiation. A buyer can explicitly commit to an offer received from the seller, or explicitly commit to the acceptance (from the seller) of an offer the buyer has sent; the seller implicitly commits when the buyer does. The negotiation ends successfully when the buyer sends an "accept" message, which it can do in response to the seller's acceptance of the buyer's offer, in response to the seller's counter offer (skipping the commitment stage) or from the committed state.

The main difference which allows this to be used as a viable concurrent protocol is the ability for participants to exit, and for the state of the negotiation to be held (by using the "commit" action) allowing the participants to continue with other negotiations simultaneously. The point which must be given consideration is that decommitments must have some associated penalty - this could be in the form of a lost deposit, or damage to reputation, but it should be taken account of in a fully-fledged environment. In the initial release of ConBiNe, it is ignored to simplify the strategies; given as how buyers are looking to purchase only one item, and then leave the marketplace, it will be likely that the former penalty (lost deposit) will be implemented, given as how each buyer has an upper-limit on how much it is authorised to spend, and lost reputation points will be inconsequential as it will only send "decommit" messages just before it must leave and/or accept a committed offer.

Figures \ref{fig:basic-trace} and \ref{fig:multi-trace} show two example traces - the former demonstrates a simple interaction, and the latter provides an example of decommitment.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/basic_trace.pdf} 
        \caption{Basic Trace}
        \label{fig:basic-trace}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/multi_trace.pdf} 
        \caption{Concurrent Trace}
        \label{fig:multi-trace}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Overview of the ConBiNe in GOLEMlite}
\texttt{ConBiNe} was used to drive the initial featureset baked into \texttt{GOLEMlite}. An overview of the implementation is given below.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Agents}
An overview of the different classes of agents are given here, but each individual type is discussed in more detail in Section \ref{sec:conbine-agents}.

% --------------------------------------------------------------------------- %

\subsubsection{Application Agents}
The only application agents in \texttt{ConBiNe} are the \texttt{MarketAgents}, both buyers and sellers, which enter the marketplace to achieve their tasks. There are a few buyers with pre-selected strategies (referred to as "selected buyers") which are always added to the marketplace. In addition to this, there are other buyers with randomly selected strategies (referred to as "competitors", though strictly speaking, a competitor in this domain is, from the point of view of a given buyer, any other buyer which is negotiating with the same seller), and sellers with randomly selected strategies. From the point of view of a given \texttt{MarketAgent} in a particular negotiation, the "opponent" is the other participant in the negotiation.

% --------------------------------------------------------------------------- %

\subsubsection{Infrastructure Agents}
\label{sec:infagents}
\texttt{ConBiNe} includes the \texttt{MarketController}, an infrastructure agent that initialises \texttt{MarketAgents} (creates them with the parameters they need to understand their goals, and starts their execution thread), adding them to the marketplace and regulating conditions within the marketplace to ensure the ratio of buyers to sellers, and density of \texttt{MarketAgents} in the marketplace remains at the required levels. The \texttt{MarketController} sets up \texttt{runs}, and cleans up after \texttt{runs}, finally cleaning up the environment at the end of an experiment by stopping the execution of every agent, itself included. 

In the initial plan, there also existed \texttt{MetricsCollectors} - agents that would parse the \texttt{ContainerHistory} at the end of each \texttt{run} to calculate metrics (see Section \ref{sec:collect-metrics}) for a selected buyer, triggered the exit of said buyer from the marketplace. In the current implementation, they have been removed for performance reasons (and although these reasons are no longer applicable, it may be that they are reintroduced).

% --------------------------------------------------------------------------- %

\subsubsection{Hybrid Agents}
In order to facilitate the matchmaking of \texttt{MarketAgents} (to form negotiations), we employ a \texttt{MarketBroker}, that listens for the entry of each \texttt{MarketAgent} to the marketplace, and sends a list of seller IDs to buyers entering the marketplace, or the seller ID to buyers already in the marketplace if the new \texttt{MarketAgent} is a seller. While it would be possible to create an application without the \texttt{MarketBroker}, where each \texttt{MarketAgent} in the marketplace is notified when a new agent enters, it's more efficient for notifications to be sent when there is the possibility of multiple products existing the marketplace at a given time, and also simplifies the creation and provisioning of unique dialogue IDs.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Marketplace}
The domain in \texttt{ConBiNe} is represented by a marketplace (\texttt{DefaultContainer}), in which \texttt{MarketAgents} (that is, implementations of \texttt{AbstractBuyerAgent} and \texttt{AbstractSellerAgent}) are trading using the concurrent alternating offers protocol (see Section \ref{sec:caop}).

Infrastructure agents (Section \ref{sec:infagents}), like the \texttt{MarketController}, will see the marketplace as a \texttt{Container}, whereas \texttt{MarketAgents} will see it as an \\\texttt{Environment} (an interface with methods that form a subset of \texttt{Container}).
 
As this application doesn't demand the state of any entity to be maintained beyond that which the entities maintain themselves, it is sufficient to use the supplied \texttt{DefaultContainer} - the only component of the marketplace is the \\\texttt{ContainerHistory}, which is part of all \texttt{Containers}.

The marketplace constitutes the simplest case, and though it isn't required, it is possible to extend the \texttt{DefaultContainer} (or implement a \texttt{Container} from scratch), such that it would be able to keep track of the environment's state, as something distinct from the sum of the states of all entities within it. An example to demonstrate this concept would be the absolute position (on a Cartesian plane) of an object in the environment - there may be no need for the object to maintain its position in the world, but it could be required that the environment should know where the object is, in the case of agents querying the location of that object, or in the case of active perception, where the environment should return all objects within a set radius of some focus point. In this instance, an extension of the \texttt{DefaultContainer} would be required to keep track of this information.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Governor}
The marketplace is governed by \texttt{ConbinePhysics}, which is very lenient - it will allow any action to occur as long as the action is of a recognised type (a type which is enumerated in the class), without checking whether or not the initiating agent is allowed to perform that action (e.g. whether it is their turn in the exchange, or whether that action is allowed according to the protocol). This is where protocol policies could be enforced, e.g. an agent cannot start the negotiation with an "accept" action.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Agents}
\label{sec:conbine-agents}
\subsection{MarketController}
The \texttt{MarketController} is an infrastructure agent that manages all of the simulations which will be run as part of the overall experiment. Each simulation is repeated a fixed number of times, and then the \texttt{MarketController} tests a different combination of variables, with the overall goal of covering every combination of variables a fixed number of times, so that the user can analyse the collected metrics, and determine the effect of each variable (or the combined effect of many variables) on the outcome or success of a given set of strategies.

The \texttt{MarketController} is responsible for initialising the simulation, for creating and starting all required agents, stopping and removing all agents (when they leave the marketplace, when they have achieved their goal or at the end of a simulation), flushing the \texttt{ContainerHistory} to a file, then starting up the next simulation (until all simulations have been covered). When all simulations have been run, the \texttt{MarketController} should stop and remove all agents, including infrastructure agents, like the \texttt{MarketBroker}, and finally terminate itself (stop its own execution cycle).

Summary of responsibilities:

\begin{itemize}
    \item create and initialise \texttt{MarketBroker} (Section \ref{sec:marketbroker})
    \item keep track of current scenario (combination of variables) (Section \ref{sec:vars})
    \item initialise each \texttt{run}, creating \texttt{MarketAgents} (Section \ref{sec:marketagents})
    \item during each \texttt{run}, control number of \texttt{MarketAgents} in marketplace
    \item after each \texttt{run}, remove all \texttt{MarketAgents}, save history to file, clear history. Start the next \texttt{run} or end the experiment if all the scenarios have been run
\end{itemize}

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{MarketBroker}
\label{sec:marketbroker}
The \texttt{MarketBroker} is an infrastructure agent that matches buyers with sellers. The end-users don't need to consider this agent when they use the testbed; it's used as an information broker for buyer agents. It is similar to \texttt{JADE's} DF agent.

For it to effectively do its job, it subscribes to events of an "announce" type - \texttt{MarketAgents} will announce themselves when they enter the marketplace, specifying their class (buyer or seller) as well as the product they are dealing with (buyers - the product they are looking to buy, and sellers - the products they have in stock). In the typical case, each buyer and seller may be dealing with several products each (for example, the buyer has a shopping list, and the seller is a general-purpose store) - in the simplest case, each \texttt{MarketAgent} will deal only with a single product type, though many different product types may exist within the marketplace; to ensure comparison between buyer strategies is fair, all sellers have an unlimited stock and there is only reference to a single product type.

The \texttt{MarketBroker} maintains two registers internally: a list of buyers and a list of sellers. When a buyer enters the marketplace, the \texttt{MarketBroker} will send that buyer the ID of every seller that deals with the product the buyer is after. When a new seller enters the marketplace, it will send that seller's ID to every buyer that has registered interest in purchasing the product that the new seller has.

The \texttt{MarketBroker} is also subscribed to "exit marketplace" actions, so it knows when to update its registers if an agent leaves - meaning no stale IDs are sent. If a stale (or even incorrect) ID is sent, it shouldn't matter; the buyer will send an initial offer, either the agent responds or doesn't - there is no error (at least, in the supplied buyer base class, \texttt{AbstractBuyerAgent}).

Summary of responsibilities:

\begin{itemize}
    \item maintain a register of all \texttt{MarketAgents} in the environment
    \item when a new buyer enters, notify them about existing sellers
    \item when a new seller enters, notify the existing buyers
\end{itemize}

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{MarketAgents}
\label{sec:marketagents}
It is possible for a single strategy to accommodate the logic required to behave as both a buyer and seller, for example, eBay users, who can assume both of these roles (though only one of these roles can be assigned to a particular user for the duration of each transaction/negotiation - for a given auction, there is one user who is the seller (and cannot be a buyer, without both the use of a separate user account and also breaking the rules), and potentially many other users that may only act in the role of a buyer (and similarly cannot simultaneously be a seller)).

In contrast to this, where a single user may be a buyer and a seller (in different transactions), the initial version of \texttt{ConBiNe} will only recognise each \texttt{MarketAgent} instance as being of type buyer or seller (\texttt{MarketAgent.AgentType\#BUYER},\\ \texttt{MarketAgent.AgentType\#SELLER}), not both. This classification is made at the time the agent is created, and remains for the entire duration of that agent's life. This implementation choice was made to simplify the interactions between \texttt{MarketAgents} and also simplify the base behaviours of \texttt{MarketAgents}; if required, another \texttt{MarketAgent} can be initialised using the same strategy, but using the other classification.

% --------------------------------------------------------------------------- %

\subsubsection{Buyers}
Buyers are \texttt{MarketAgents} with the goal of maximising utility per negotiation, where a successful negotiation is one in which they make a purchase of the product they are looking for. The typical buyer will remain in the marketplace until they reach a successful negotiation, until they are told to leave by the \texttt{MarketController}, or until they reach their internal deadline.

Summary of responsibilities:

\begin{itemize}
    \item initiate negotiation with every seller the \texttt{MarketBroker} notifies the buyer about
    \item leave the marketplace when a negotiation is successful (agreement reached), terminating all other negotiations
    \item leave the marketplace when the deadline is reached
    \item leave the marketplace when the \texttt{MarketController} requests it
\end{itemize}

% --------------------------------------------------------------------------- %

\subsubsection{Sellers}
Sellers are \texttt{MarketAgents} with the goal of maximising utility per negotiation, where a successful negotiation (utility is over 0) is one in which they make a sale of a product to a buyer. Each seller will remain in the marketplace until the end of the simulation, until they are told to leave by the \texttt{MarketController}, or if their strategy dictates they must leave (e.g. because their inventory is insufficient to remain useful in the marketplace).

Summary of responsibilities:

\begin{itemize}
    \item respond to offers from buyers
    \item terminate negotiation when deadline is reached
    \item leave the marketplace when the \texttt{MarketController} requests it
\end{itemize}

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{ConBiNe Variables}
\label{sec:vars}
To allow the user to supply input parameters, a series of generic \textit{variable} classes were written to facilitate a common interface between different types of input. Using these, it would be possible to accommodate agents which require parameters that are not known at the time of building the application; it is up to the end-user not to supply parameters which give these agents an unfair advantage over previously developed agents (namely, prior knowledge).

One of the front-facing classes includes \texttt{ConbineVariable}, which is an abstract class representing a set of values related to a particular concept. The set of values is comprised of intervals (\texttt{ValueBuckets}), such as "slow, average, fast", or "small, medium, large", etc.

Each \texttt{ValueBucket} (e.g. "small") represents a set of discrete values\\(\texttt{DiscreteVariable}), or two points between which a value can be picked at random (\texttt{ContinuousVariable}). The value types currently accepted include ratios (discrete only), as well as integers and doubles (discrete and continuous).

As an example, to represent the number of \texttt{MarketAgents} in the marketplace at any given time, we define a \texttt{ConbineVariable} shown in Table \ref{tbl:densityVar} (to see the corresponding code, refer to the Appendix, Section \ref{sec:apdx-vars}).   

\begin{table}
\centering
    \begin{tabular}{|l|l|l|}
    \hline
    sparse   & 1  & 5  \\ \hline
    average  & 6  & 15 \\ \hline
    busy     & 16 & 25 \\ \hline
    \end{tabular}
    \caption {density: defining a ContinuousVariable with 3 intervals}
    \label{tbl:densityVar}
\end{table}


The number of ranges we can set is arbitrary. Using the example given in Table \ref{tbl:densityVar}, "density.average" will return an integer between 6 and 15 inclusive.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{ConBiNe Run}
A \texttt{ConBiNe} \texttt{run} is dependent on the variables the end-user inputs. For each \texttt{run}, the parameters are determined by the \texttt{MarketController} based on what the user has entered. There are fixed variables that must be entered by the user (or defaults will be chosen), including:

\begin{itemize}
    \item Market density change rate 
    \item Market ratio change rate
    \item Buyer/seller type pool
    \item Selected buyers
    \item Market change time
\end{itemize}

as well as a set of basic parameters which most \texttt{MarketAgents} expect, in order to be initialised correctly:

\begin{itemize}
    \item Buyer/seller initial price
    \item Buyer/seller reservation price
    \item \texttt{MarketAgent} deadline
\end{itemize}

\textit{Market Density Change Rate} - this \texttt{DiscreteVariable} represents the number of \texttt{MarketAgents} within the environment at any given time point. More than this, it is used to represent the rate at which \texttt{MarketAgents} are entering and exiting the marketplace.

Each \texttt{DiscreteValueBucket} in this represents a different rate of change, and should contain several integer values which justify that \texttt{ValueBucket's} identifier. For example, the experiment we set up includes three different rates of change: quick, average and slow. For the "quick" \texttt{ValueBucket}, we choose the values "30", "40" and "50". The values themselves are unimportant, but rather the difference between them (10) is relatively steep, when compared with the "slow" \texttt{ValueBucket}, which has values of "8", "10" and "12" (a much smaller difference). There may be an arbitrary number of values in each \texttt{ValueBucket}, but there should be at least two in order to give any meaning to the variable. The value chosen represents the \textit{total} number of \texttt{MarketAgents} in the marketplace, but does not give any constraints on the composition of agent types - this responsibility falls to the next variable.

\textit{Market Ratio Change Rate} - this \texttt{DiscreteVariable} represents the ratio of buyer \texttt{MarketAgents} to sellers, and can be thought of as representing the demand-supply dynamic within the marketplace. The value chosen from this is used in combination with the density variable to get the number of buyer agents that should be in the marketplace, and the number of seller agents that should be in the marketplace.

\textit{Buyer/Seller Type Pool} - when the \texttt{MarketController} needs to add new competitors and sellers to the marketplace to "fill the quota", the type pools are where it gets the strategies for which to initialise the \texttt{MarketAgents} that it adds, selecting randomly from these pools. In \texttt{ConBiNe}, we decided to limit the type pool such that it didn't include any of the very complex agents (the ones we were to collect performance metrics for) in order to ease any potential strain the system might have encountered unnecessarily.

\texttt{ConBiNe} is used to collect metrics on the performance of different buyer strategies. The "Selected Buyers" are the strategies to collect metrics for. A simulation only ends when \textit{all} of the selected buyers leave the marketplace (either because they reached their goal or because they reached their deadline).

\textit{Market Change Time} - the \texttt{MarketController} modifies the market at fixed time points - new competitors and sellers are added to the market, and existing sellers and competitors are told to terminate their negotiations and leave the market (it is assumed all \texttt{MarketAgents} comply), according to the market density change rate and market ratio change rate. This demonstrates an \textit{open} environment, where participants are not necessarily known at design time. It is at this time that the \texttt{MarketController} reselects the values for each of the market variables (from the same \texttt{ValueBuckets} though). While initially the plan was to have three different market change times (2, 5 and 10 seconds), it was decided that 2 and 5 seconds were too short for any meaningful interactions to have taken place, and so we settled on a fixed value of 10. This was achieved by only supplying one \texttt{DiscreteValueBucket} to the market change time \texttt{DiscreteVariable}. More can be added by the user if they wish to analyse the effects of a longer period of stability than 10 seconds.

\textit{Buyer/seller initial price} - refers to the value that all buyers should use as their initial price, and that all sellers should initially "advertise" their product at. In reality, buyers won't all have the same initial prices (similarly for sellers) but as the point of the experiment is to compare the effectiveness of each strategy, it is fairer to give each \texttt{MarketAgent} the same initial parameters. The "Buyer/seller reservation price" is similar: the buyer reservation price is the maximum price it is willing to pay for an item. The seller reservation price is the minimum price it is willing to sell the item for. As with the buyer/seller initial price, all buyers have the same reservation, and all sellers have the same reservation, for the same reasons as specified previously.

\textit{MarketAgent Deadline} - the deadline for all \texttt{MarketAgents}. Again, all buyers should have the same deadline, and all sellers should have the same deadline, for the reasons as specified above. In this case however, we also gave the same deadline to both buyers \textit{and} sellers. This is because we are collecting metrics on whether the deadline has any effect on the outcome - by splitting the deadline into buyer deadline and seller deadline, we introduce two variables instead of one, and increase the number of variable combinations that must be tested. The buyer's "timer" starts the moment that it enters the marketplace. When its deadline is reached, it must leave the marketplace; the default behaviour is to check if there are any committed offers - if there are, choose one at random to accept after decommitting from the rest, otherwise exit. The seller has many timers - one per negotiation. Each timer starts from the moment it receives the first offer from the buyer. If the timer reaches the deadline, the seller will exit from that negotiation; this is to ensure that the seller gives each buyer a fair chance, and also to cull any unresponsive negotiation threads.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Metrics}
\subsection{Collecting Metrics}
\label{sec:collect-metrics}
As metrics for each domain, and even each application within a given domain, will vary enormously, the responsibility of developing a mechanism to collect metrics is given to the application developer. In \texttt{ConBiNe}, we allow the user to select which metrics should be calculated from a pre-defined list (for the selected buyers only). The metrics are calculated as an average over the number of repeated runs, and these averages are plotted against the different variables. For example, it will be possible to see how a particular buyer strategy fared as the deadline increased. The metrics we allow the user to select from are as follows:

\begin{itemize}
    \item number of successful runs
    \item average overall utility of a buyer
    \item average utility of a buyer (over successful runs)
    \item buyer's average time spent in the marketplace
\end{itemize}

\textit{Successful runs} - the number of runs where a negotiation ended in an "accept" action from the buyer. The maximum value this can be is equal to the number of repetitions per variable combination.

\textit{Average utility} - the sum of the utility from each run, divided by the number of repetitions per variable combination. The utility ranges from 0, which indicates an unsuccessful negotiation, to 1.0, which indicates the buyer was able to acquire the product using its initial bid.

\textit{Average utility over successful runs} - the sum of the utility from each run, divided by the number of successful runs. The utility ranges from 0, which indicates an unsuccessful negotiation, to 1.0, which indicates the buyer was able to acquire the product using its initial bid.

\textit{Time spent in marketplace} - the sum of the time that the buyer spent in the marketplace, divided by the number of repetitions per variable combination. This shows the average amount of time the buyer spends in the marketplace, where a value close to 0 indicates the buyer achieves its goal (a successful negotiation) quickly, and a value close to 1 indicates the buyer either achieves its goal slowly, or not at all. Note, the \texttt{MarketController} will never request that a selected buyer leave the marketplace; if a selected buyer leaves the marketplace without achieving its goal, it is because it has reached its deadline, and no other reason.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Processing Metrics}
At the end of each run, the \texttt{MarketController} saves a copy of the \\\texttt{ContainerHistory} to a text file. In the case of \texttt{ConBiNe}, there should be 2700 files in total, as there are 100 runs per combination of variables, and there are 27 different combinations of variables.

We ran scenarios based on the following variables:

\begin{enumerate}
    \item MarketDensityChangeRate
    \item MarketRatioChangeRate
    \item \texttt{MarketAgent} Deadline
\end{enumerate}

of which there were three intervals each, hence the 27 different combinations of variables.

Per selected buyer, we parse the history file to determine the metrics listed in Section \ref{sec:collect-metrics}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/conbineexample.pdf} 
        \caption{ConBiNe Experiment Example}
        \label{fig:conbineexample}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

\section{Summary}
The \texttt{ConBiNe} application was a challenge to develop at the same time as \texttt{GOLEMlite} - while we understood the requirements of the \texttt{ConBiNe} multi-agent system, it was not clear how they would be represented in \texttt{GOLEMlite} until after the features were implemented (in the framework), and then had to be tweaked, while attempting not to leak any of \texttt{ConBiNe's} implementation into the underlying platform - \texttt{GOLEMlite} had to be kept domain-independent.

Figure \ref{fig:conbineexample} shows an example of an experimental setup. All the agents are in the same \texttt{Container}, with the \texttt{MarketController} sitting on the top "layer" as an infrastructure agent. The lines from the \texttt{MarketController} to the history and registry show direct access, whereas the dotted lines from \texttt{MarketBroker} indicate that it \textit{has} access but doesn't exercise these privileges. \texttt{MarketAgents} are not even aware that these objects exist. The diagram also shows multiple concurrent bilateral dialogues taking place (labelled "d1" to "d7").

It was through \texttt{ConBiNe} though that we were able to identify and solve many issues, including the requirement to have an \texttt{AgentBrain} that was compatible with thread pooling (discussed in Section \ref{sec:changesFromConbine}) to avoid memory leaks, and a \\\texttt{Sensor}/\texttt{Effector} implementation that could be extended by each application as necessary (which we achieved through the use of compatible \texttt{ActionTypes}).
















