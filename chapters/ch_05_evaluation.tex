\chapter{Evaluation}
\label{chap:evaluation}

\section{Keeping the API Simple}
Building a simple agent-based application using \texttt{GOLEMlite} has clear cut steps (when compared to \texttt{GOLEM}). Simply put:

\begin{enumerate}
    \item Create an \texttt{Application} class
    \item Specify a \texttt{Container} for the agents
    \item Create agents (made using \texttt{AgentBrain}, \texttt{AgentMind}, and \texttt{AgentBody})
    \item Register \texttt{Sensors} and \texttt{Effectors} to the \texttt{AgentBody}
    \item Add the agents to the \texttt{Container}
    \item Start the agents
\end{enumerate}

\texttt{GOLEMlite} tries to provide default implementations of many classes, or at least, base classes where most of the required functionality is given, taking a "convention over configuration" approach where the application developer should have to specify as little as possible, unless it deviates from the convention (provided functions). The workflow above in more detail:

\begin{enumerate}
    \item Create an class extending \texttt{AbstractApplication}
    \item Override the \texttt{\#launch()} method - in it, initialise a \texttt{Container} and add your agents
    \item Use the \texttt{DefaultContainer} and \texttt{DefaultPhysics} if the environment doesn't require modification (e.g. if application only contains speech-based agents)
    \item Create an agent: supplying an \texttt{AgentMind}, \texttt{AgentBrain} and \texttt{AgentBody}
    \item Extend \texttt{AbstractAgentMind} to create a Java-based mind
    \item Override \texttt{\#executeStep()} - the typical flow is to retrieve all new \texttt{Percepts} from the brain, process them as needed, and return a \texttt{List} of \texttt{Actions}
    \item For speech-based agents, \texttt{DefaultAgentBrain} and \texttt{DefaultAgentBody} are fine, though abstract implementations are provided as base classes
    \item Register \texttt{Sensors} and \texttt{Effectors} to the \texttt{AgentBody}. This is as simple as creating a \texttt{Sensor} (extend \texttt{AbstractSensor}) and specifying the type of \texttt{Actions} the \texttt{Sensor} can interpret using \texttt{addType(String)}, e.g. \\\texttt{this.addType(ActionType.SPEAK)}. Register this \\\texttt{Sensor} using \texttt{AgentBody\#registerSensor(Sensor)}. An identical process for \texttt{Effectors} is required
    \item Make each agent "present" in the \texttt{Container}, with \\\texttt{Container\#makePresent(Entity)}
    \item For each agent, call \texttt{AgentBrain\#startExecution()}
\end{enumerate}

The part of this workflow that provides the least satisfaction includes the implementation of the \texttt{Sensors} and \texttt{Effectors} - the only thing application developers have to modify when they create these, are the \texttt{ActionTypes} they are capable of working with, so the amount of work required for this is disproportionately large. In this situation, there are several options available:

Add a Builder to \texttt{Sensor} and \texttt{Effector}, which lets the developer add \texttt{ActionTypes}, and returns the compiled component on \texttt{\#build}
Add a register method to \texttt{AgentBody} which creates and adds \texttt{Sensors}/\texttt{Effectors} on behalf of the developer, e.g. \\\texttt{body.registerEffector(ActionType.SPEAK)}

It is important to retain the \texttt{registerSensor(Sensor)} (similarly for \texttt{Effector}) method as part of the grand plan is to supply a library of pre-built \texttt{Sensors}. Currently, it is difficult to justify separate classes of \texttt{Sensors} based on the \texttt{ActionTypes} they support; in all other respects, all \texttt{Sensors} behave identically. It is likely however, that eventually, more complex \texttt{Sensors} will be built, which require non-trivial processing for an \texttt{Action} to be interpreted, such as an encrypted message, or an audio stream.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

% Produce a framework (lighter than \texttt{GOLEM}) which can be used to develop multi-agent systems, based on concepts upon which \texttt{GOLEM} was built
\section{Based on GOLEM Concepts}
In general, many of the concepts remain similar to \texttt{GOLEM} - there are still \texttt{Containers}, \texttt{Physics} layers, and agents are still represented as a set of components: mind, body (sensors and effectors) and brain. \texttt{GOLEMlite} refines this approach by refactoring responsibilities (e.g. the brain now controls the agent thread, instead of the body), providing clear interfaces, and base classes which can be extended by application developers. For simple applications, default implementations are also provided which can be used as-is (or extended further).

In an attempt to simplify development of the framework, we removed the networking feature of \texttt{GOLEM} - multiple \texttt{Containers} can be created within a single \texttt{Application} but it is not possible for agents to communicate between them. Agent communication in \texttt{GOLEM} (in fact, any message passing, which included any interactions on the environment) occurred via assertions to the Prolog database, and notifications to subscribed entities, with Ambient Event Calculus driving the process. \texttt{GOLEMlite} allows the use of AEC and a Prolog-based \texttt{Physics}, but it is not implemented. Instead, we opted to use a POJO (Plain Old Java Object) as the history of the \texttt{Container}, for the purpose of keeping the default implementations as simple as possible.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

% Demonstrate the framework in action by building a non-trivial application (Chapter \ref{chap:conbine})
\section{Changes Driven by ConBiNe}
\label{sec:changesFromConbine}
The \texttt{GOLEM} framework only supposes a small number of agents being created at a given time - the PacketWorld application \cite{bromuri2008situating} would slow down noticeably if assertions in the Prolog database grew to a large number, and frequent queries were required. In addition, there was no built-in support for agents that would be able to run using a fixed size thread pool. The \texttt{ConBiNe} application required an open marketplace, where agents would be coming and leaving; during testing, the number of agents that were being created grew to thousands during a \texttt{run}. The memory overhead with spawning so many threads lead to the application crashing, so it was necessary to use a fixed size thread pool - Java's \texttt{ExecutorService}\footnote{\url{http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Executors.html\#newFixedThreadPool(int)}}. This was created using the maximum number of \texttt{MarketAgents} that would possibly be in the environment at any given time (based on the \texttt{Market Density} variable). \texttt{DefaultAgentBrain} managed its own thread though, so it was necessary to create a new implementation of \texttt{AgentBrain}, the \texttt{DefaultPoolableAgentBrain} as shown in Figure \ref{fig:poolbrain}.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{figures/DefaultPoolableAgentBrain.png} 
        \caption{Class diagram showing DefaultPoolableAgentBrain}
        \label{fig:poolbrain}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Initially, \texttt{AgentMind\#executeStep()} would only get called when there were new \texttt{Percepts}; during the development of \texttt{ConBiNe} it was realised this was not sufficient - \texttt{MarketAgents} needed to have \texttt{\#executeStep()} called regardless, so it could be allowed to check the current time against their deadlines, and \textit{initiate} \texttt{Action} based on this, and not just in reaction to a \texttt{Percept} as was previously the case.

\texttt{ConBiNe} is the first application developed in \texttt{GOLEMlite} with practically useful infrastructure agents - this prompted the introduction of \texttt{Environment} as a restricted interface to the \texttt{Container}.

In \texttt{GOLEM}, there was no strict enforcement of when an agent was allowed to perform actions - \texttt{CommodityMind} in the \texttt{GOLEM} project shows actions being initiated in the middle of the mind's \texttt{\#executeStep()} method. For \texttt{GOLEMlite}, the mind cannot instigate \texttt{Actions} directly, but must return a list of \texttt{Actions} at the \textit{end} of the \texttt{\#executeStep()} function. This makes it easier to develop base classes, for example, a base class for a Prolog-based mind, as otherwise it would be difficult to initiate \texttt{Actions} at indiscriminate points from within the reasoning step. It also makes more organisational sense: an agent should decide the actions it will take \textit{after} it has finished reasoning (processing), so this is the last part of the cycle step.
