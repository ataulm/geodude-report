\chapter{Background}
\label{chap:background}

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
\section{Intelligent Agents and Multi Agent Systems}
\label{sec:iamas}
The following presents an overview to the agents concepts, including an explanation of what constitutes an "agent", and discusses the concept of an "environment" in the context of agents-programming.

\subsection{Agents}
There is "no universally accept definition of the term agent" \cite{wooldridge2008introduction}, but for the purposes of this thesis, the following definition is acceptable based on the author's experience and understanding of the concept: agents are software entities, situated in some environment, that perform actions to achieve some goal (usually, on behalf of another entity, like a human user).

An intelligent agent is an agent which can formulate plans, by dividing its goals into subgoals to fulfil the overarching task on behalf of the aforementioned entity.

\subsubsection{Agent Architectures}
There exist several classes of agent, and a common classification defines four groups \cite{russell1995artificial} (though there also exist other, more detailed, classifications with more groupings), based on some perceived measure of intelligence:

\begin{itemize}
    \item Simple Reflex Agents
    \item Model-based Reflex Agents
    \item Goal-based Agents
    \item Utility-based Agents
\end{itemize}

\textit{Simple Reflex Agents} don't maintain a history of the environment, nor of their previous actions. The next action of a Simple Reflex Agent is solely dependent on the last sensed \textit{percept}. These are the simplest of all agents to conceptualise and create. An example of a Simple Reflex Agent would include a thermostat controller, switching on the central heating depending on the difference between the current temperature and the user-set temperature, or the internet service, IFTTT\footnote{\url{https://ifttt.com/wtf}} (If This Then That), where the user is able to specify conditions and the reactions to those conditions (e.g. "if I'm tagged in a photo on Facebook, send me a text message"). These are called condition action rules in AI.

\textit{Model-based Reflex Agents} retain a model about how the environment works - the natural processes occurring in the environment, and an understanding of how the agent's actions will affect the environment. In addition to this, the agent maintains an internal state of the environment, and the combination of these two properties in the model allow it to update its internal state of the entire environment, which is useful when the environment is only partially \textit{accessible} (see Section \ref{sec:bg-env-properties}).

\textit{Goal-based Agents}
It is often not enough to estimate the current state of the environment to decide on the next action - goal-based agents make predictions based on their understanding of the environment and how it changes. Instead of being entirely reflex-based, goal-based agents reason about whether future states are desirable, choosing actions which are likely to lead to such a state.

\textit{Utility-based Agents}
An extension to goal-based agents, utility-based agents assign a value representing the desirability of a particular outcome on a relative scale, from 0 to 1, where 0 is entirely undesirable and 1 denotes absolute success. Utility-based agents will predict the effect of their actions on the state of the environment, calculate the utility of the states, and choose the action which is most likely to lead to the state with the highest scoring utility.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Environments}
\label{sec:bg-env}
An environment is the surrounding context of an agent; the area or world in which it is physically situated. Agents work by sensing the state of the environment (or \textit{changes} to the \textit{existing} state), and effecting actions in the environment via actuators. The state of the agent environment at any given point consists of the collective state of all agents and objects within the environment at that given point.

% --------------------------------------------------------------------------- %

\subsubsection{Environment Properties}
\label{sec:bg-env-properties}

The following properties classifying a given particular environment are proposed by Russell and Norvig \cite{russell1995artificial}.

\textit{Accessibility} - an accessible environment is an environment for which it is possible to get up-to-date information about the state. It also refers to whether information about the entire state is available, or just part of it.

\textit{Determinism} - the determinism of an environment relates to whether an action will produce a knowable effect on the state. The real world is an example of an environment which is non-deterministic; it is very difficult (computationally unfeasible) to know what effect a given action will have on the state. Deterministic environments are likely to be extremely simple, with limited variables, and potentially many assumptions.

\textit{Dynamism} - a static environment is one in which changes to the environment are effected by the actions of agents only - there are no other forces in play. The opposite to a static environment is a dynamic environment (again, like the real world).

\textit{Discrete and Continuous} - a discrete environment is one in which there are a finite number of states, e.g. a tic tac toe game. On the other hand, a continuous environment has "uncountably many states" \cite{wooldridge2008introduction} (not necessarily infinite).


% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\subsection{Multi-agent Systems}
While an agent may function on its own within a system, it is also typical to expect multiple agents to be working in the same environment (with different goals). It is not enough for multiple agents to be working in the same environment to be considered a multi-agent system; a multi-agent system defines a system with an architecture capable of allowing inter-agent communication - intelligent agents must be aware of other agents, and be able to communicate with them, for the purposes of cooperation or competition, when required. The key concept in a multi-agent system (or MAS) is \textit{interaction}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Existing Agent Platforms}
\label{sec:platforms}

This section introduces several existing agent platforms, chosen as they seem to be actively developed, as well as popular among the agent development community. It is not expected that \texttt{GOLEMlite} will match the following in functionality and quality at this stage in its development, but adoption of \texttt{GOLEMlite} will invariably be influenced by how well it compares to the state of the art; these should be used to identify the standard that must be reached (and then beaten).

\subsection{JADE}
\texttt{JADE} (\textbf{J}ava \textbf{A}gent \textbf{D}evelopment \textbf{F}ramework) \cite{bellifemine1999jade} is a Java-based middleware for creating distributed (peer-to-peer) multi-agent systems.

\texttt{JADE} agents are compliant with FIPA (\textbf{F}oundation for \textbf{I}ntelligent \textbf{P}hysical \textbf{A}gents) ACL (\textbf{A}gent \textbf{C}ommunication \textbf{L}anguage) specifications, a proposed standard language for agent communication. Agents in \texttt{JADE} run in \textit{containers}, which can be run on different physical machines (a distributed architecture), where a given collection of active containers constitutes a \textit{platform}. In a platform, the first container is designated as the \textit{main container}, with which all others must register; only one main container is allowed - the creation of a second main container implies the creation of a second platform.

As long as containers are correctly set up (registered with the main container), then agents are able to communicate with one another, regardless of their physical location, in the same way if they are in the same container, as when they are in different containers; it is transparent to the agents themselves.

The main container contains two "special" agents which are automatically created: the AMS (Agent Management System) and the DF (Directory Facilitator). All \texttt{JADE} agents must register with the AMS before they are able to communicate with other agents; the AMS ensures that agents are registered with unique identifiers, and has the ability to create or kill agents on remote containers. While \texttt{JADE} agents are able to communicate with the AMS, users new to \texttt{JADE} will typically interact with the AMS via a GUI. The DF allows agents to advertise their services; other agents can query the DF, looking for a service provider, and the DF will return a list of matching agents.\footnote{\url{http://jade.tilab.com/doc/tutorials/JADEProgramming-Tutorial-for-beginners.pdf}}

\subsection{JASON}
\texttt{Jason} \cite{bordini19overview} is an Java-based interpreter for programs written using an extended version of AgentSpeak(L) \cite{Rao96agentspeak(l):bdi}, an agent-oriented programming language based on the \textit{Beliefs-Desires-Intentions} (BDI) architecture. It has gained traction for being the "first fully-fledged interpreter for a much improved version of AgentSpeak, including also speech-act based inter-agent communication"\footnote{\url{http://jason.sourceforge.net/wp/description/}}.

The ability to communicate affords \texttt{Jason} the power to be used to create multi-agent systems. It works with both (either) \texttt{JADE} or \texttt{Saci}\footnote{\url{http://www.lti.pcs.usp.br/saci/index.shtml}} to provide over-the-network communication, allowing distributed multi-agent systems.


\subsection{JaCaMo}
\texttt{JaCaMo} \cite{boissier2011multi} is the product derived from the amalgamation of three different projects:

\begin{itemize}
    \item \texttt{Jason}
    \item \texttt{Moise} - Moise Organisation Oriented Programming Framework\footnote{\url{http://moise.sourceforge.net/}}
    \item \texttt{CArtAgO} (Common ARTifact infrastructure for AGents Open environments)\footnote{\url{http://cartago.sourceforge.net/}}
\end{itemize}

\texttt{JaCaMo} aims to cover "all levels of abstractions" for the development of multi-agent platforms, using \texttt{Jason} for programming agents, and \texttt{CArtAgO} for the programming of environments and \texttt{Moise} as an organisational model, which contains concepts like roles, groups, and missions \cite{Hübner02amodel}.

% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{GOLEM}
\label{sec:bg-golem}
\texttt{GOLEM}\footnote{\url{http://golem.cs.rhul.ac.uk/}} is Java/Prolog-based multi-agent system middleware, backed by Ambient Event Calculus \cite{bromuri2009distributed} for its environment implementation. It represents the environment as a logic-based theory (using Prolog facts), which allows the perception of the environment to be equalavent to "importing parts of a logical theory, while action execution is implemented as a transaction over a distributed and complex logical structure".

\begin{quote}
"The main GOLEM entities that can be deployed to support practical applications are Containers, Agents, Objects, Processes and Services. Agents are active entities involved in social and physical interactions inside the environment while objects are passive resources that encapsulate a reactive behaviour. Both agents and objects are situated inside the environment. Processes are modelled as ongoing activity in the agent environment: an example of process is a force applied to an object or a pheromone evaporating. Services are provided to the agents at the environment level and they enable the platform to be distributed, as well as providing registries for the resources and topologies for the environment." - \textit{\texttt{GOLEM} website}
\end{quote}

An emphasis is given on defining the environment \cite{golem:eemas07} as a logical structure that evolves over time; changes state. The state changes are governed by a set of physical laws - the Physics component checks whether actions are possible, and if so, asserts them, then notifies all subscribed entities that such an action has been asserted as an event. \texttt{GOLEM} agents operate in \textit{containers}, which can be nested (containers within containers), and run on separate machines, with communication between containers occuring over a network.



% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %
% --------------------------------------------------------------------------- %

\section{Summary}
\label{sec:bg-summary}
This chapter introduces us to the concept of intelligent agents and multi-agent systems. We become familiar with different classes of agents that may need to be represented in a multi-agent system (and thus, the underlying framework must be capable of representing such agents), and classifications of agent environments using properties, which may affect how we group, contain and situate agents.

A standard representing the quality of the state of the art is set, with the understanding that \texttt{GOLEMlite} should aspire towards reaching the same standard, and then raising the standard, measured by developer adoption of the framework in the agents community.

Finally, we look at \texttt{GOLEM} as a platform, discussing the main components (\textit{containers}, \textit{agents}, \textit{objects}, \textit{processes} and \textit{services}), for which we will provide direct or indirect analogues to in \texttt{GOLEMlite}, if they prove necessary to the creation of a \textit{basic} implementation of \texttt{GOLEM} concepts.





