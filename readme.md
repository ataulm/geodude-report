# Thesis

## Intro
- Motivation (summary of current GOLEM problems)
- Aims and objectives (how I plan to address)
- Contribution
- Outline - structure of the thesis

## Background
- Agents
- Environments
- GOLEM as it stands
- Other platforms with their interesting ideas

## Framework
    
## Implementation

## Evaluation
- show runs with Bedour

## Conclusion & future work
## References
## Appendices


The `LaTeX` template for this thesis was taken from `Metathesis v2.0` (see below).

---

## Metathesis v2.0

These files comprise a template for using LaTeX to typeset a
graduate thesis (MSc or PhD) at Memorial University of Newfoundland
(http://www.mun.ca/).  This template is bested suited for a Linux
environment which has recent versions of latex, ghostscript, make and
associated packages installed.

This template may be useful at other institutions for other operating
systems as well.

Start with thesis.tex and modify the other files as required.  For more
information on usage and the latest version of the template, please
consult the web page:

[Metathesis Home](http://www.cs.mun.ca/~donald/metathesis/)
